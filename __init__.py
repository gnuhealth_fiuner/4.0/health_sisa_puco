# -*- coding: utf-8 -*-
#This file is part health_sisa_puco module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.

from trytond.pool import Pool
from . import party
from .wizard import party_puco
from .wizard import patient_puco


def register():
    Pool.register(
        party.Party,
        party.Identifier,
        party_puco.PucoDataStart,
        patient_puco.PatientPucoDataStart,
        module='health_sisa_puco', type_='model')
    Pool.register(
        party_puco.PucoData,
        patient_puco.PatientPucoData,
        module='health_sisa_puco', type_='wizard')
