# This file is part of GNU Health SISA PUCO module.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.exceptions import UserError


class ErrorWS(UserError):
    pass


class ErrorAutenticacion(UserError):
    pass


class ErrorInesperado(UserError):
    pass


class NoCuotaDisponible(UserError):
    pass


class ErrorDatos(UserError):
    pass


class RegistroNoEncontrado(UserError):
    pass


class MultipleResultado(UserError):
    pass


class MoreThanOnePatient(UserError):
    pass
