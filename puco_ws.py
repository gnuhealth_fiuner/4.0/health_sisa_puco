# -*- coding: utf-8 -*-
#This file is part health_sisa_puco module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
try:
    import lxml.etree as etree
except ImportError:
    import xml.etree.cElementTree as etree
import requests

from trytond.pool import Pool
from trytond.transaction import Transaction


class PucoWS(object):
    'PUCO Web Service'

    url_test = 'https://sisaqa.msal.gov.ar/sisaqa/services/rest/puco'
    url = 'https://sisa.msal.gov.ar/sisa/services/rest/puco'
    headers = {'content-type': 'application/json'}

    @classmethod
    def get_xml(cls, id_number):
        User = Pool().get('res.user')

        user = User(Transaction().user)
        payload = '{"usuario": "%s", "clave": "%s"}' % (user.sisa_user,
            user.sisa_password_hidden)
        url = cls.url
        if user.sisa_mode == 'testing':
            url = cls.url_test
        url += '/' + id_number

        try:
            response = requests.post(url, data=payload, headers=cls.headers,
                timeout=30, verify=False)
            return etree.fromstring(response.content)
        except Exception:
            return None
