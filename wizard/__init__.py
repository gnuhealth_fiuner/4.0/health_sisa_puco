# -*- coding: utf-8 -*-
#This file is part health_sisa_puco module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
from . import party_puco
from . import patient_puco
