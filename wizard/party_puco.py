# -*- coding: utf-8 -*-
#This file is part health_sisa_puco module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
from ..puco_ws import PucoWS

from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, StateTransition, Button
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.i18n import gettext

from ..exceptions import (ErrorWS, ErrorAutenticacion, ErrorInesperado,
    NoCuotaDisponible, ErrorDatos, RegistroNoEncontrado, MultipleResultado,
    MoreThanOnePatient)

class PucoDataStart(ModelView):
    'PUCO Data Start'
    __name__ = 'party.puco_data.start'

    cobertura_social = fields.Char('Insurance party', readonly=True)
    rnos = fields.Char('RNOS code', readonly=True)
    numero = fields.Char('Number')
    denominacion = fields.Char('Name', readonly=True)
    tipodoc = fields.Char('ID type', readonly=True)
    nrodoc = fields.Char('ID number', readonly=True)


class PucoData(Wizard):
    'PUCO Data'
    __name__ = 'party.puco_data'

    start = StateView(
        'party.puco_data.start',
        'health_sisa_puco.puco_data_start_view', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'update_party', 'tryton-ok', default=True),
        ])
    update_party = StateTransition()

    def default_start(self, fields):
        Party = Pool().get('party.party')

        res = {}
        party = Party(Transaction().context['active_id'])
        if not party:
            return res
        xml = PucoWS.get_xml(party.ref)
        if xml is None:
            raise ErrorWS(gettext('msg_error_ws'))
        if xml.findtext('resultado') == 'OK':
            cobertura = []
            osocial = {}
            for cober in xml.findall('puco'):
                osocial['nombreObraSocial'] = cober.findtext('coberturaSocial')
                osocial['rnos'] = cober.findtext('rnos')
                osocial['denominacion'] = cober.findtext('denominacion')
                osocial['tipodoc'] = cober.findtext('tipodoc')
                osocial['nrodoc'] = cober.findtext('nrodoc')
                osocial['procedencia'] = 'puco'
                cobertura.append(osocial)
                osocial = {}
            res = {
                'cobertura_social': cobertura[0]['nombreObraSocial'],
                'rnos': cobertura[0]['rnos'],
                'denominacion': cobertura[0]['denominacion'],
                'tipodoc': cobertura[0]['tipodoc'],
                'nrodoc': cobertura[0]['nrodoc'],
            }
        elif xml.findtext('resultado') == 'ERROR_AUTENTICACION':
            raise ErrorAutenticacion(gettext('msg_error_autenticacion'))
        elif xml.findtext('resultado') == 'ERROR_INESPERADO':
            raise ErrorInesperado('msg_error_inesperado')
        elif xml.findtext('resultado') == 'NO_TIENE_QUOTA_DISPONIBLE':
            raise NoCuotaDisponible(gettext('msg_no_cuota_disponible'))
        elif xml.findtext('resultado') == 'ERROR_DATOS':
            raise ErrorDatos(gettext('msg_error_datos'))
        elif xml.findtext('resultado') == 'REGISTRO_NO_ENCONTRADO':
            raise RegistroNoEncontrado(gettext('msg_registro_no_encontrado'))
        elif xml.findtext('resultado') == 'MULTIPLE_RESULTADO':
            raise MultipleResultado(gettext('msg_multiple_resultado'))

        return res

    def transition_update_party(self):
        pool = Pool()
        Party = pool.get('party.party')
        Insurance = pool.get('gnuhealth.insurance')

        party_id = Transaction().context.get('active_id')
        numero = self.start.numero if self.start.numero else self.start.nrodoc

        insurance_party = Party().search([
            ('is_insurance_company', '=', True),
            ('identifiers.type', '=', 'ar_rnos'),
            ('identifiers.code', '=', self.start.rnos),
            ])
        if not insurance_party:
            insurance_party = Party().search([
                ('is_insurance_company', '=', True),
                ('name', '=', self.start.cobertura_social),
                ])

        if insurance_party:
            insurance = Insurance().search([
                ('name', '=', party_id),
                ('company', '=', insurance_party[0]),
                ])
            if insurance:
                return 'end'

            insurance_data = {
                'name': party_id,
                'number': numero,
                'company': insurance_party[0],
                'insurance_type': insurance_party[0].insurance_company_type,
                }
            Insurance.create([insurance_data])

        return 'end'
